// ignore_for_file:
// https://dart.dev/codelabs/async-await

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void printMessage(String message) {
  if (kDebugMode) {
    print(message);
  }
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: 'MyHomePage',
      routes: {
        MyHomePage.routeName: (context) => const MyHomePage(
              title: 'Flutter Demo Home Page',
            ),
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  static const routeName = 'MyHomePage';
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget asyncAndAwaitWithTryCatchElevatedButton() {
    return ElevatedButton(
      onPressed: () {
        // TryCatchWithError().main();
      },
      child: const Text('async and await with try-catch'),
    );
  }

  Widget createElevatedButton(String title) {
    return ElevatedButton(
      onPressed: () {
        switch (title) {
          case 'Incorrectly using an asynchronous function':
            IncorrectlyUsingAnAsynchronousFunction().main();
            break;
          case 'Introducing futures':
            IntroducingFutures().main();
            break;
          case 'Completing with an error':
            CompletingWithAnError().main();
            break;
          case 'Execution within async functions':
            ExecutionWithinAsyncFunctions().main();
            break;
          case 'async and await with try-catch':
            AsyncAndAwaitWithTryCatch().main();
            break;
          default:
            break;
        }
      },
      child: Text(
        title,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              createElevatedButton(
                  'Incorrectly using an asynchronous function'),
              createElevatedButton('Introducing futures'),
              createElevatedButton('Completing with an error'),
              createElevatedButton('Execution within async functions'),
              createElevatedButton('async and await with try-catch'),
              FutureBuilder<int>(
                future: delayNumber(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Text(
                      snapshot.error.toString(),
                    );
                  }
                  if (snapshot.hasData) {
                    return Text(
                      snapshot.data.toString(),
                    );
                  }
                  return const Text(
                    'Waiting for result',
                  );
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        // onPressed: () async {
        //   try {
        //     var result = await delayNumberWithError();
        //     print(result);
        //   } catch (e) {
        //     print(e.toString());
        //   }
        //   print("run here");
        // },

        onPressed: () async {
          // fetchUserOrder();
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

Future<int> delayNumberWithError() {
  return Future.delayed(
      const Duration(seconds: 2), () => throw Exception('Có lỗi xảy ra'));
}

Future<int> delayNumber() {
  return Future.delayed(const Duration(seconds: 2), () => 1);
}

class IncorrectlyUsingAnAsynchronousFunction {
  // This example shows how *not* to write asynchronous Dart code.

  String createOrderMessage() {
    var order = fetchUserOrder();
    return 'Your order is: $order';
  }

  Future<String> fetchUserOrder() =>
      // Imagine that this function is more complex and slow.
      Future.delayed(
        const Duration(seconds: 2),
        () => 'Large Latte',
      );

  void main() {
    printMessage(createOrderMessage());
  }
}

class IntroducingFutures {
  Future<void> fetchUserOrder() {
    // Imagine that this function is fetching user info from another service or database.
    return Future.delayed(
        const Duration(seconds: 2), () => printMessage('Large Latte'));
  }

  void main() {
    fetchUserOrder();
    printMessage('Fetching user order...');
  }
}

class CompletingWithAnError {
  Future<void> fetchUserOrder() {
// Imagine that this function is fetching user info but encounters a bug
    return Future.delayed(const Duration(seconds: 2),
        () => throw Exception('Logout failed: user ID is invalid'));
  }

  void main() {
    fetchUserOrder();
    printMessage('Fetching user order...');
  }
}

class ExecutionWithinAsyncFunctions {
  Future<void> printOrderMessage() async {
    printMessage('Awaiting user order...');
    var order = await fetchUserOrder();
    printMessage('Your order is: $order');
  }

  Future<String> fetchUserOrder() async {
        printMessage('Awaiting user order 2...');
        await Future.delayed(const Duration(seconds: 1));
    // Imagine that this function is more complex and slow.
            printMessage('response 2...');

    return Future.delayed(const Duration(seconds: 4), () => 'Large Latte');
  }

  Future<void> main() async {
    countSeconds(5);
    await printOrderMessage();
  }

// You can ignore this function - it's here to visualize delay time in this example.
  void countSeconds(int s) {
    for (var i = 1; i <= s; i++) {
      printMessage('vao for cua countSeconds $i');
      Future.delayed(Duration(seconds: i), () => printMessage(i.toString()));
    }
  }
}

class AsyncAndAwaitWithTryCatch {
  Future<void> printOrderMessage() async {
    try {
      printMessage('Awaiting user order...');
      var order = await fetchUserOrder();
      printMessage(order);
    } catch (err) {
      printMessage('Caught error: $err');
    }

    printMessage('Run here');
  }

  Future<String> fetchUserOrder() {
    // Imagine that this function is more complex.
    var str = Future.delayed(
        const Duration(seconds: 4), () => throw 'Cannot locate user order');
    return str;
  }

  Future<void> main() async {
    research();
    printMessage('asyn study');
    await printOrderMessage();
  }

  bool predicate(String item) {
    return item.length > 5;
  }

  void research() {
    const items = ['Salad', 'Popcorn', 'Toast', 'Lasagne'];

    // You can find with a simple expression:
    var foundItem1 = items.firstWhere((item) => item.length > 5);
    printMessage(foundItem1);

    // Or try using a function block:
    var foundItem2 = items.firstWhere((item) {
      return item.length > 5;
    });
    printMessage(foundItem2);

    // Or even pass in a function reference:
    var foundItem3 = items.firstWhere(predicate);
    printMessage(foundItem3);

    // You can also use an `orElse` function in case no value is found!
    var foundItem4 = items.firstWhere(
      (item) => item.length > 10,
      orElse: () => 'None!',
    );
    printMessage(foundItem4);
  }
}
